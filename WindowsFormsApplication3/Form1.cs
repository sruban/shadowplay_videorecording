﻿using System;
using System.Windows.Forms;
using ScapLIB;
using System.Drawing;
using System.IO;
namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    { 
            public static int startPosX ;
            public static  int startPosY ;
           // Size cursize = new Size();
           public static int  RecWidth;
           public static int Recheight;
          

        //ScapCapture Cap = new ScapCapture(true, 5);
        ScapCapture Cap = new ScapCapture(true, 5, 30, ScapVideoFormats.WMV2, ScapImageFormats.Jpeg, startPosX, startPosY, RecWidth, Recheight);

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ScapCore.StartCapture();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ScapCore.StopCapture();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ScapCore.DecompressCapture(false);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ScapCore.EncodeCapture(false);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = "Frame Count: " + ScapCore.GetCaptureCount().ToString();
            label2.Text = "FPS: " + ScapCore.GetActualFPS().ToString();
            DecompProgressBar.Value = (int)(ScapCore.GetDecompressionProgress() * 100);
            EncodeProgressBar.Value = (int)(ScapCore.GetEncodeProgress() * 100);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ScapBackendConfig.ScapBackendSetup(Cap);
            timer1.Interval = 10;
            timer1.Enabled = true;
            DecompProgressBar.Maximum = 100;
            EncodeProgressBar.Maximum = 100;
        }

        private void selectscreen_Click(object sender, EventArgs e)
        {
            captureSelectScreen();
            
        }
        public void captureSelectScreen()
        {

            Point curPos = new Point(Cursor.Position.X, Cursor.Position.Y);
            int startPosX = Cursor.Position.X;
            int startPosY = Cursor.Position.Y;
            Size cursize = new Size();
            RecWidth = Cursor.Current.Size.Width;
            Recheight = Cursor.Current.Size.Height;

            System.Console.WriteLine("THIS IS CURSOR STARTPOSX---" + startPosX);
            System.Console.WriteLine("THIS IS CURSOR sTARTPOSY---" + startPosY);
            System.Console.WriteLine("THIS IS RECTANGLE WIDTH---" + RecWidth);
            System.Console.WriteLine("THIS IS RECANTGLE HEIGHT---" + Recheight);

            //{
            //    ScapCapture capture = new ScapCapture(true, 5, 30, ScapVideoFormats.WMV2, ScapImageFormats.Jpeg, startPosX, startPosY, cursize.Width, cursize.Height);

            //}
        }
    }
}
